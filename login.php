<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

// Configuración de la base de datos
$dbHost = 'localhost';
$dbName = 'login';
$dbUser = 'postgres';
$dbPassword = 'mimteg123Pos';

try {
    $db = new PDO("pgsql:host=$dbHost;dbname=$dbName", $dbUser, $dbPassword);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error de conexión a la base de datos: " . $e->getMessage());
}

function writeLog($message) {
    // Specify the name of the log file
    $logFile = "./logfile.log";

    // Get the current date and time
    $now = date("Y-m-d H:i:s");

    // Format the log message
    $logMessage = $now . " - " . $message . "\n";

    // Write the message to the log file
    file_put_contents($logFile, $logMessage, FILE_APPEND);

}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];

    // Consulta para verificar el usuario
    $stmt = $db->prepare("SELECT nombre, apellido, password FROM users WHERE nombre_usuario = :usuario");
    $stmt->bindParam(':usuario', $usuario);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    #var_dump($result);
    #exit();

    if ($result && password_verify($password, $result['password'])) {
        // Inicio de sesión exitoso
        $_SESSION['nombre'] = $result['nombre'];
        $_SESSION['apellido'] = $result['apellido'];
        writeLog("User " . $_POST['usuario'] . " logged in successfully.");
        header('Location: welcome.php');
        
    } else {
        // Error de inicio de sesión
        writeLog("Failed login attempt for user " . $_POST['usuario'] . ".");
        $error = "Credenciales incorrectas. Inténtalo de nuevo.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <div class="login-container">
        <h1>Iniciar Sesión</h1>
        <form action="login.php" method="post">
            <label for="usuario">Usuario:</label>
            <input type="text" name="usuario" required>
            <label for="password">Contraseña:</label>
            <input type="password" name="password" required>
            <input type="submit" value="Iniciar Sesión">
        </form>
        <?php if (isset($error)) { ?>
            <p class="error"><?php echo $error; ?></p>
        <?php } ?>
    </div>
</body>
</html>