<?php
session_start();
if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Bienvenido</title>
    <link rel="stylesheet" type="text/css" href="./styles.css">
</head>
<body>
    <div class="login-container">
        <h1>Bienvenido <?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellido']; ?></h1>
        <a href="./logout.php">Cerrar Sesión</a>
    </div>
</body>
</html>
