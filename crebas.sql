CREATE TABLE IF NOT EXISTS public.users
(
    id serial,
    nombre character varying(50) COLLATE pg_catalog."default",
    apellido character varying(50) COLLATE pg_catalog."default",
    nombre_usuario character varying(50) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    PRIMARY KEY (id)
);